import config as cfg
import os
from tqdm import tqdm
import time
from parser import parse_in_chunks, decode_row
from collections import defaultdict


def create_csv_name(old_name, partition, delimiter="^"):
    new_name = old_name.name.split(delimiter)
    new_name[1] = str(partition)
    return new_name[0] + delimiter + new_name[1] + delimiter + new_name[2]


def write_csv(f_write, data, curr_lines, f_read):
    if curr_lines == cfg.MAX_LINES:
        pbar.update(f_read.tell() - pbar.n)          # update progress bar
        f_write.close()
        f_write = open(create_csv_name(f_write, next(iter(data))), 'w')
        curr_lines = 0

    for key, value in data.items():
        f_write.write(key+' ' + ','.join(value) + '\n')

    return f_write, curr_lines


def parse_file(filename):
    curr_lines = 0
    f_write = open(cfg.FILENAME, 'w', newline='')
    categories = defaultdict(list)
    with open(filename, 'rb') as f_read:
        for parsed_chunk in parse_in_chunks(f_read, cfg.CATEGORY_REGEX, cfg.CATEGORY_TRASH):
            key, value = decode_row(parsed_chunk)

            if key not in categories and len(categories) >= cfg.DICT_SIZE:
                f_write, curr_lines = write_csv(f_write, categories, curr_lines, f_read)
                categories = defaultdict(list)
                curr_lines += cfg.DICT_SIZE
            else:
                categories[key].append(value)


# GB
if __name__ == '__main__':
    file_size = os.path.getsize(cfg.CATEGORY_FILENAME)
    with tqdm(total=file_size, desc='File read progress', position=0, leave=True) as pbar:
        parse_file(cfg.CATEGORY_FILENAME)
