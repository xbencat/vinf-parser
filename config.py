CATEGORY_TRASH = 1927  # Unimportant data length at the beginning of the file, that can be skipped while parsing
PAGE_TRASH = 2168
CHUNK_SIZE = 512  # Size of chunk to read from file
CATEGORY_REGEX = rb'\((.*?),\'(.*?)\'.*?\)'
PAGE_REGEX = rb'\((.+?),\d,\'(.*?)\'.*?\)'
FILENAME = 'category_files/categories^0^.txt'
MAX_LINES = 20000
DICT_SIZE = 100
DIR = "/home/bencat/PycharmProjects/School/vinf-parser/category_files/*.txt"
CATEGORY_FILENAME = '/home/bencat/Downloads/enwiki-latest-categorylinks.sql'
PAGE_FILENAME = '/home/bencat/Downloads/enwiki-latest-page.sql'
INDEX_SETTINGS = {
    "settings": {
        "index": {
            "number_of_shards": 1,
            "number_of_replicas": 1
        },
        "analysis": {
            "analyzer": {
                "my_analyzer": {
                    "tokenizer": "my_tokenizer"
                }
            },
            "tokenizer": {
                "my_tokenizer": {
                    "type": "simple_pattern_split",
                    "pattern": "_"
                }
            }
        }
    },
    "mappings": {
        "properties": {
            "page": {
                "analyzer": "my_analyzer",
                "type": "text"
            },
            "categories": {
                "analyzer": "my_analyzer",
                "type": "text"
            }
        }
    }
}
