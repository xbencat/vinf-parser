import glob
import os
import json
import ast

from tqdm import tqdm

import config as cfg
from parser import parse_in_chunks, decode_row
from elasticsearch import Elasticsearch, helpers


def create_index(es_object):
    created = False
    # index settings
    try:
        if not es_object.indices.exists(es_index):
            # Ignore 400 means to ignore "Index Already Exist" error.
            es_object.indices.create(index=es_index, ignore=400, body=cfg.INDEX_SETTINGS)
            print('Created Index')
        created = True
    except Exception as ex:
        print(str(ex))
    finally:
        return created


def iterate(iterable):
    iterator = iter(iterable)
    item = next(iterator)

    for next_item in iterator:
        yield item, next_item
        item = next_item

    yield item, None


def get_categories(page_id):
    filename = get_filename(page_id)
    with open(filename) as f:
        for line in f:
            category_id, categories = line.split(' ', 1)
            if int(category_id) == page_id:
                return categories.rstrip().split(',')


def parse_file(file):
    json_bulk = []
    with open(file, 'rb') as f_read:
        for parsed_chunk in parse_in_chunks(f_read, cfg.PAGE_REGEX, cfg.PAGE_TRASH):
            pbar.update(f_read.tell() - pbar.n)
            row = decode_row(parsed_chunk)
            categories = get_categories(int(row[0]))
            json_data = {
                '_index': es_index,
                '_source': {'id': int(row[0]), 'page': row[1], 'categories': categories}
            }
            json_bulk.append(json_data)
            if len(json_bulk) == 1000:
                helpers.bulk(es, json_bulk)
                json_bulk = []
    helpers.bulk(es, json_bulk)


def file_map(directory):
    files = dict()
    for file in glob.glob(directory):
        key = file.split('^')[1]
        files[key] = file
    return dict(sorted(files.items(), key=lambda x: int(x[0])))


def get_filename(page_id):
    for curr, next in iterate(category_files.items()):
        if int(curr[0]) <= page_id < int(next[0]):
            return curr[1]


if __name__ == '__main__':
    es_index = 'wiki_categories'
    es_doctype = 'wiki_record'
    es = Elasticsearch()
    create_index(es)
    file_size = os.path.getsize(cfg.PAGE_FILENAME)
    category_files = file_map(cfg.DIR)
    with tqdm(total=file_size, desc='File read progress', position=0, leave=True) as pbar:
        parse_file(cfg.PAGE_FILENAME)
