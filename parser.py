import config as cfg
import re


def parse_in_chunks(file, regex, trash_size):
    is_trash = True     # dont yield trash
    buffer = b''

    while True:
        if is_trash:
            file.read(trash_size)
            is_trash = False
            continue
        else:
            data = file.read(cfg.CHUNK_SIZE)
        if not data:    # EOF
            break
        buffer += data

        parts = re.findall(regex, buffer)

        for part in parts:
            # if its not a digit, its some random stuff from category description
            if part[0].isdigit():
                yield b' '.join(part)

        # save the part after last regex match, prevent from losing data
        buffer = buffer[buffer.find(b",'" + part[1] + b"'") + len(part[1]):]


def decode_row(data):

    try:
        data = data.decode("utf-8")
    except UnicodeDecodeError:
        return ''

    return data.split(' ', 1)
